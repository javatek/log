package org.bitbucket.javatek.log;

import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class LogManager {
  private LogManager() {}

  public static void shutdown() {
    ((LoggerContext) LoggerFactory.getILoggerFactory()).stop();
  }
}
