package org.bitbucket.javatek.log;

import ch.qos.logback.classic.PatternLayout;

import static ch.qos.logback.core.CoreConstants.LINE_SEPARATOR;

/**
 *
 */
public final class PatternLayoutWithFooter extends PatternLayout {
  private static final String FOOTER = ""
    + LINE_SEPARATOR
    + "********************************************************************************"
    + LINE_SEPARATOR
    + LINE_SEPARATOR;

  public PatternLayoutWithFooter() {
    setFileFooter(FOOTER);
  }
}
